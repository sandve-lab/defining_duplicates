#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --job-name=defining_duplicates
#SBATCH --partition=smallmem,orion,gpu


module load Anaconda3 # Has pandoc installed
module load R/4.0.4 # Correct R version
module list

Rscript -e "Sys.setenv(RSTUDIO_PANDOC='/usr/lib/rstudio-server/bin/pandoc'); \
rmarkdown::render('defining_duplicates.Rmd', output_file='defining_duplicates.html')"
