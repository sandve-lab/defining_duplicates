# Download the Ensembl106 compara gene trees and the corrected SCORPiOs trees from aqua-faang

DOWNLOAD_DIR=/mnt/SCRATCH/lagr/Compara106

mkdir $DOWNLOAD_DIR
cd $DOWNLOAD_DIR

wget http://ftp.ensembl.org/pub/misc/aqua-faang/ensembl_trees/gene_trees/Compara.106.protein_default.nhx.emf.gz
wget http://ftp.ensembl.org/pub/misc/aqua-faang/ensembl_trees/scorpio_corrected_trees/106_PostSCORPiOs.tgz

tar xvf 106_PostSCORPiOs.tgz